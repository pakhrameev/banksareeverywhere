# README #

Banks list in Russian cities.

Test task to show my current skills in iOS programming (but using Interface Builder with AutoLayout).

### Task ###

Application to show cities and banks list in every city.

1. Cities request - http://seekrate.ru/api/city.php
2. Banks in selected city request http://seekrate.ru/api/v3.php?&city=<filial_city> (filial_city is from city entity)

Application should have 2 screens: cities list (in table) and banks list (in table).
Third party libraries are forbidden. UI should be as simple as possible.
It's necessary to use xib/storyboard and AutoLayout.