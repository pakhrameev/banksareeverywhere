//
//  UPKBankFilial.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKBankFilial.h"

#import "UPKBank.h"
#import "NSDictionary+SimpleJSON.h"


#pragma mark - Locals

static NSString *const kIdentifierKey = @"banks_id";
static NSString *const kAddressKey = @"hintContent";
static NSString *const kPhoneKey = @"contact";


#pragma mark - UPKBankFilial implementation

@implementation UPKBankFilial


#pragma mark - Properties

@synthesize bank = m_bank;
@synthesize displayName = m_displayName;
@synthesize address = m_address;
@synthesize phone = m_phone;

- (nonnull NSString *) identifier
{
  return self.bank.identifier;
}

- (nonnull NSString *) displayName
{
  return [NSString stringWithFormat: @"%@ %@", self.bank.displayName, self.address];
}

#pragma mark - Inits

- (nullable instancetype) initWithDictionary: (nonnull NSDictionary *) dictionary
                                    allBanks: (nonnull NSDictionary <NSString *, UPKBank *> *) banks;
{
  NSString *identifier  = [dictionary stringFromPath: kIdentifierKey];
  NSString *address     = [dictionary stringFromPath: kAddressKey];
  NSString *phone       = [dictionary stringFromPath: kPhoneKey];
  
  if (identifier.length == 0 || address.length == 0 || phone.length == 0)
  {
    return nil;
  }
  
  UPKBank *bank = [banks objectForKey: identifier];
  
  if (bank == nil)
  {
    return nil;
  }
  
  if (self = [super init])
  {
    m_bank = bank;
    m_address = [address copy];
    m_phone = [phone copy];
  }
  
  return self;
}


@end
