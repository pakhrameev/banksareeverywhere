//
//  UPKBank.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

@interface UPKBank : NSObject

@property (nonatomic, copy, readonly, nonnull) NSString *displayName;
@property (nonatomic, copy, readonly, nonnull) NSString *identifier;

- (nullable instancetype) initWithDictionary: (nonnull NSDictionary *) dictionary;

- (nullable instancetype) init UNAVAILABLE_ATTRIBUTE;

@end
