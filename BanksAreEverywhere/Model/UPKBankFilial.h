//
//  UPKBankFilial.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

@class UPKBank;

@interface UPKBankFilial : NSObject

@property (nonatomic, strong, readonly, nonnull) UPKBank *bank;
@property (nonatomic, copy, readonly, nonnull) NSString *displayName;
@property (nonatomic, copy, readonly, nonnull) NSString *identifier;
@property (nonatomic, copy, readonly, nonnull) NSString *address;
@property (nonatomic, copy, readonly, nonnull) NSString *phone;

/**
 *  Initializes UPKBankFilial instance with properties from dictionary and 
 *  array of banks to search specified bank in it
 *
 *  @param dictionary Dictionary to fill properties with.
 *  @param banks Array of UPKBank's to find correct instance to link with. Don't mutate it during init.
 *
 *  @return Result UPKBankInstance with all correct fields or nil (if something missed).
 */
- (nullable instancetype) initWithDictionary: (nonnull NSDictionary *) dictionary
                                    allBanks: (nonnull NSDictionary <NSString *, UPKBank *> *) banks;

- (nullable instancetype) init UNAVAILABLE_ATTRIBUTE;

@end
