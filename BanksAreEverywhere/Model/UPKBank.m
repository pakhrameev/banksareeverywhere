//
//  UPKBank.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKBank.h"
#import "NSDictionary+SimpleJSON.h"


static NSString *const kIdentifierKey = @"banks_id";
static NSString *const kDisplayNameKay = @"balloonContent";


@implementation UPKBank

@synthesize identifier  = m_identifier;
@synthesize displayName = m_displayName;

- (nullable instancetype) initWithDictionary: (nonnull NSDictionary *) dictionary
{
  NSString *identifier  = [dictionary stringFromPath: kIdentifierKey];
  NSString *displayName = [dictionary stringFromPath: kDisplayNameKay];
  
  if (identifier.length == 0 || displayName.length == 0)
  {
    return nil;
  }
  
  if (self = [super init])
  {
    m_identifier  = [identifier copy];
    m_displayName = [displayName copy];
  }
  
  return self;
}


@end
