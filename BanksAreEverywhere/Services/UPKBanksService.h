//
//  UPKBanksService.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

@class UPKCity;
@class UPKBankFilial;

extern const NSUInteger kInvalidRequestId;
extern const NSInteger kInternalServerErrorCode;
extern const NSInteger kNoInternetConnectionErrorCode;
extern const NSInteger kCanceledErrorCode;
extern const NSInteger kUnknownErrorCode;

extern  NSString * _Nonnull const kBanksServiceErrorDomain;

typedef void (^CitiesCompletion) (NSArray <UPKCity *> * _Nullable, NSUInteger, NSError  * _Nullable );
typedef void (^BankFilialsCompletion) (NSArray <UPKBankFilial *> * _Nullable, NSUInteger, NSError * _Nullable);

@interface UPKBanksService : NSObject

- (NSUInteger) requestCitiesWithCompletion: (nonnull CitiesCompletion) completion;
- (NSUInteger) requestBankFilialsWithCity: (nonnull UPKCity *) city
                            andCompletion: (nonnull BankFilialsCompletion) completion;


@end
