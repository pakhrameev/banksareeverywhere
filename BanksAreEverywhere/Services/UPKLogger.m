//
//  UPKLogger.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKLogger.h"

@implementation UPKLogger

- (void) logWithMethod: (const char *) method
                  line: (int) line
                format: (NSString *) format, ...
{
  NSMutableString *string = [[NSMutableString alloc] initWithUTF8String: method];
  [string appendFormat: @":%d ", line];
  va_list args;
  va_start(args, format);
  NSString *formatString = [[NSString alloc] initWithFormat: format
                                                  arguments: args];
  va_end(args);
  [string appendString: formatString];
  NSLog(@"%@", string);
}

@end
