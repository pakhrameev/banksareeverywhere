//
//  UPKServiceLocator.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKServiceLocator.h"

#import "UPKBanksService.h"
#import "UPKLocalizationManager.h"
#import "UPKURLManager.h"
#import "UPKLogger.h"


#pragma mark - Local

static UPKServiceLocator *s_serviceLocator = nil;


#pragma mark - UPKServiceLocator Internal Interface

@interface UPKServiceLocator ()
{
  NSRecursiveLock *m_serviceLock;
  
  UPKBanksService *m_banksService;
  UPKLocalizationManager *m_localizationManager;
  UPKURLManager *m_urlManager;
  UPKLogger *m_logger;
}

@end


#pragma mark - UPKServiceLocator Implementation

@implementation UPKServiceLocator

#pragma mark - Inits

- (instancetype) init
{
  self = [super init];
  if (self)
  {
    m_serviceLock = [[NSRecursiveLock alloc] init];
  }
  
  return  self;
}

#pragma mark - Public Methods

+ (UPKServiceLocator *) sharedLocator
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    s_serviceLocator = [[UPKServiceLocator alloc] init];
  });
  
  return s_serviceLocator;
}

- (UPKBanksService *) banksService
{
  [m_serviceLock lock];
  if (!m_banksService)
  {
    m_banksService = [[UPKBanksService alloc] init];
  }
  [m_serviceLock unlock];
  
  return m_banksService;
}

- (UPKLocalizationManager *) localizationManager
{
  [m_serviceLock lock];
  if (!m_localizationManager)
  {
    m_localizationManager = [[UPKLocalizationManager alloc] init];
  }
  [m_serviceLock unlock];
  
  return m_localizationManager;
}

- (UPKURLManager *) urlManager
{
  [m_serviceLock lock];
  if (!m_urlManager)
  {
    m_urlManager = [[UPKURLManager alloc] init];
  }
  [m_serviceLock unlock];
  
  return m_urlManager;
}

- (UPKLogger *) logger
{
  [m_serviceLock lock];
  if (!m_logger)
  {
    m_logger = [[UPKLogger alloc] init];
  }
  [m_serviceLock unlock];
  
  return m_logger;
}

@end

