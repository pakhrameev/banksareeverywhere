//
//  UPKLocalizationManager.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKLocalizationManager.h"


@implementation UPKLocalizationManager

- (NSString *) localizedStringForKey: (NSString *) key
{
  return NSLocalizedStringFromTable(key, @"Localizable", nil);
}

@end
