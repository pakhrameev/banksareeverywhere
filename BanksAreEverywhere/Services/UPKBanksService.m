//
//  UPKBanksService.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKBanksService.h"

#import "UPKLocalizationManager.h"
#import "UPKURLManager.h"

#import "UPKCity.h"
#import "UPKBank.h"
#import "UPKBankFilial.h"

#import "NSURLSession+Creator.h"
#import "NSDictionary+SimpleJSON.h"


#define REPORT_ERROR(e) if (error != nil) *error = e;
#define SERVER_SIDE_ERROR_DESCRIPTION NSLocalizedDescriptionKey : UPKLocalizedString(@"SERVER_SIDE_ERROR_DESCRIPTION")


#pragma mark - External constants

const NSUInteger kInvalidRequestId = NSUIntegerMax;
const NSInteger kInternalServerErrorCode = 0;
const NSInteger kNoInternetConnectionErrorCode = 1;
const NSInteger kCanceledErrorCode = 2;
const NSInteger kUnknownErrorCode = 3;

NSString *const kBanksServiceErrorDomain = @"UPKBanksService";


#pragma mark - Locals

static NSString *const kHTTPMethodGET = @"GET";

static NSString *const kCitiesListKey = @"list";
static NSString *const kBanksListKey = @"banks";
static NSString *const kBankFilialsListKey = @"data";
static NSString *const kCityKey = @"city";


#pragma mark - UPKBanksService internal interface

@interface UPKBanksService() <NSURLSessionDelegate>

@property (nonatomic, strong) NSURLSession *URLSession;
@property (nonatomic, strong) NSMutableDictionary *requestIdToCompletionBlock;
@property (nonatomic, strong) NSLock *dictionaryAccessLock;

@end


#pragma mark - UPKBanksService implementation

@implementation UPKBanksService

#pragma mark - Properties

@synthesize URLSession = m_URLSession;
@synthesize requestIdToCompletionBlock = m_requestIdToCompletionBlock;
@synthesize dictionaryAccessLock = m_dictionaryAccessLock;

#pragma mark - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_URLSession = [NSURLSession sessionWithDelegate: self];
    m_requestIdToCompletionBlock = [NSMutableDictionary new];
    m_dictionaryAccessLock = [NSLock new];
  }
  
  return self;
}

#pragma mark - Internal methods

- (NSUInteger) generateNextRequestId
{
  static NSLock *lock = nil;
  static NSUInteger currentId = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    lock = [NSLock new];
  });
  
  NSUInteger result = 0;
  [lock lock];
  result = currentId++;
  [lock unlock];
  
  return result;
}

- (NSUInteger) generateNextRequestIdAndStoreCompletion: (id) completion
{
  NSParameterAssert(completion != nil);
  if (completion == nil)
  {
    return kInvalidRequestId;
  }
  
  NSUInteger requestId = [self generateNextRequestId];
  [self.dictionaryAccessLock lock];
  self.requestIdToCompletionBlock[@(requestId)] = [completion copy];
  [self.dictionaryAccessLock unlock];
  
  return requestId;
}

- (id) clearRequestWithId: (NSUInteger) requestId
{
  [self.dictionaryAccessLock lock];
  id result = self.requestIdToCompletionBlock[@(requestId)];
  [self.requestIdToCompletionBlock removeObjectForKey: @(requestId)];
  [self.dictionaryAccessLock unlock];
  
  return result;
}

- (void) setJSONHTTPHeadersToRequest: (NSMutableURLRequest *) request
{
  NSString *const kContentTypeFieldName = @"Content-Type";
  NSString *const kJSONContentType = @"application/json";
  
  [request setValue: kJSONContentType forHTTPHeaderField: kContentTypeFieldName];
}

#pragma mark Error helpers

- (NSError *) noConnectionError
{
  return [NSError errorWithDomain: kBanksServiceErrorDomain
                             code: kNoInternetConnectionErrorCode
                         userInfo: @{ NSLocalizedDescriptionKey: UPKLocalizedString(@"NO_INTERNET_ERROR_DESCRIPTION") }];
}

- (NSError *) canceledError
{
  return [NSError errorWithDomain: kBanksServiceErrorDomain
                             code: kCanceledErrorCode
                         userInfo: @{ NSLocalizedDescriptionKey: UPKLocalizedString(@"REQUEST_CANCELED_ERROR_DESCRIPTION") }];
}

- (NSError *) unknownError
{
  return [NSError errorWithDomain: kBanksServiceErrorDomain
                             code: kUnknownErrorCode
                         userInfo: @{ NSLocalizedDescriptionKey : UPKLocalizedString(@"UNKNOWN_ERROR_DESCRIPTION") }];
}

- (NSError *) internalServerErrorWithUserInfo: (NSDictionary *) userInfo
{
  return [NSError errorWithDomain: kBanksServiceErrorDomain
                             code: kInternalServerErrorCode
                         userInfo: userInfo];
}

- (NSError *) errorFromRequestError: (NSError *) error
{
  if (error != nil)
  {
    if ([error.domain isEqualToString: NSURLErrorDomain] && (error.code == NSURLErrorCancelled))
    {
      UPKLog(@"%@", [self canceledError].localizedDescription);
      return [self canceledError];
    }
    else if ([error.domain isEqualToString: NSURLErrorDomain] &&
             (error.code == NSURLErrorNotConnectedToInternet      ||
              error.code == NSURLErrorNetworkConnectionLost       ||
              error.code == NSURLErrorTimedOut                    ||
              error.code == NSURLErrorInternationalRoamingOff     ||
              error.code == NSURLErrorDataNotAllowed              ||
              error.code == NSURLErrorDataLengthExceedsMaximum    ||
              error.code == NSURLErrorCannotConnectToHost         ||
              error.code == NSURLErrorCannotFindHost))
    {
      UPKLog(@"%@", [self noConnectionError].localizedDescription);
      return [self noConnectionError];
    }
    
    return [self unknownError];
  }
  return nil;
}


#pragma mark - Internal parsing methods

- (NSArray <UPKCity *> *) citiesFromServerResponse: (NSData *) data
                           urlResponse: (NSURLResponse *) urlResponse
                                 error: (out NSError **) error
{
  NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData: data ? : [NSData data]
                                                             options: 0
                                                               error: nil];
  
  if (![dictionary isKindOfClass: [NSDictionary class]])
  {
    REPORT_ERROR([self internalServerErrorWithUserInfo: @{ SERVER_SIDE_ERROR_DESCRIPTION }])
    return nil;
  }
  
  NSArray *citiesList = [dictionary arrayFromPath: kCitiesListKey];
  
  NSMutableArray <UPKCity *> *mcities = [NSMutableArray new];
  for (NSDictionary *dic in citiesList)
  {
    if (![dic isKindOfClass: [NSDictionary class]])
    {
      REPORT_ERROR([self internalServerErrorWithUserInfo: @{ SERVER_SIDE_ERROR_DESCRIPTION }])
      return nil;
    }
    UPKCity *city = [[UPKCity alloc] initWithDictionary: dic];
    if (city != nil)
    {
      [mcities addObject: city];
    }
  }
  
  return [mcities copy];
}

- (NSArray *) bankFilialsFromServerResponse: (NSData *) data
                                urlResponse: (NSURLResponse *) urlResponse
                                      error: (out NSError **) error
{
  NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData: data ? : [NSData data]
                                                             options: 0
                                                               error: nil];
  
  if (![dictionary isKindOfClass: [NSDictionary class]])
  {
    REPORT_ERROR([self internalServerErrorWithUserInfo: @{ SERVER_SIDE_ERROR_DESCRIPTION }])
    return nil;
  }
  
  NSArray *banksList = [dictionary arrayFromPath: kBanksListKey];
  
  NSMutableDictionary <NSString *, UPKBank *> *mbanks = [NSMutableDictionary new];
  for (NSDictionary *dic in banksList)
  {
    if (![dic isKindOfClass: [NSDictionary class]])
    {
      REPORT_ERROR([self internalServerErrorWithUserInfo: @{ SERVER_SIDE_ERROR_DESCRIPTION }])
      return nil;
    }
    UPKBank *bank = [[UPKBank alloc] initWithDictionary: dic];
    if (bank != nil)
    {
      [mbanks setObject: bank forKey: bank.identifier];
    }
  }
  
  NSDictionary <NSString *, UPKBank *> *banks = [mbanks copy];
  
  NSArray *bankFilialsList = [dictionary arrayFromPath: kBankFilialsListKey];
  NSMutableArray <UPKBankFilial *> *mbankFilials = [NSMutableArray new];
  for (NSDictionary *dic in bankFilialsList)
  {
    if (![dic isKindOfClass: [NSDictionary class]])
    {
      REPORT_ERROR([self internalServerErrorWithUserInfo: @{  }])
      return nil;
    }
    UPKBankFilial *bankFilial = [[UPKBankFilial alloc] initWithDictionary: dic
                                                                 allBanks: banks];
    if (bankFilial != nil)
    {
      [mbankFilials addObject: bankFilial];
    }
  }
  
  return [mbankFilials copy];
}


#pragma mark - Public methods

- (NSUInteger) requestCitiesWithCompletion: (nonnull CitiesCompletion) completion
{
  const NSUInteger requestId = [self generateNextRequestIdAndStoreCompletion: completion];
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    __weak UPKBanksService *weakSelf = self;
    
    void(^requestCompletion)(id, id, id) = ^(NSData *data, NSURLResponse *response, NSError *error)
    {
      NSError *internalError = [weakSelf errorFromRequestError: error];
      NSArray *cities = nil;
      if (internalError != nil)
      {
        error = internalError;
      }
      else
      {
        NSError *e = nil;
        
        cities = [weakSelf citiesFromServerResponse: data
                                        urlResponse: response
                                              error: &e];
        if (e != nil)
        {
          error = e;
        }
      }
      
      CitiesCompletion completionBlock = [weakSelf clearRequestWithId: requestId];
      if (completionBlock != nil)
      {
        completionBlock(cities, requestId, error);
      }
    };
    
    NSURL *url = [URL_MANAGER banksCitiesURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    [request setHTTPMethod: kHTTPMethodGET];
    
    [[self.URLSession dataTaskWithRequest: request completionHandler: requestCompletion] resume];
  });
  
  return requestId;
}

- (NSUInteger) requestBankFilialsWithCity: (UPKCity *) city
                            andCompletion: (BankFilialsCompletion) completion
{
  const NSUInteger requestId = [self generateNextRequestIdAndStoreCompletion: completion];
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    __weak UPKBanksService *weakSelf = self;
    
    void(^requestCompletion)(id, id, id) = ^(NSData *data, NSURLResponse *response, NSError *error)
    {
      NSError *internalError = [weakSelf errorFromRequestError: error];
      NSArray *bankFilials = nil;
      if (internalError != nil)
      {
        error = internalError;
      }
      else
      {
        NSError *e = nil;
        
        bankFilials = [weakSelf bankFilialsFromServerResponse: data
                                                  urlResponse: response
                                                        error: &e];
        if (e != nil)
        {
          error = e;
        }
      }
      
      CitiesCompletion completionBlock = [weakSelf clearRequestWithId: requestId];
      if (completionBlock != nil)
      {
        completionBlock(bankFilials, requestId, error);
      }
    };
    
    NSURL *url = [URL_MANAGER banksFilialsURLWithParameters: @{ kCityKey : city.identifier }];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    [request setHTTPMethod: kHTTPMethodGET];
    
    [[self.URLSession dataTaskWithRequest: request completionHandler: requestCompletion] resume];
  });
  
  return requestId;
}

@end
