//
//  UPKLogger.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@interface UPKLogger : NSObject

/**
 *  The main and only log method to track the events in the application.
 *
 *  @param method The name of Method called the event.
 *  @param line   The line of code called the method.
 *  @param format The usual ObjC format line with parameters. May contain no params.
 */
- (void) logWithMethod: (const char *) method
                  line: (int) line
                format: (NSString *) format, ...;

@end
