//
//  UPKLocalizationManager.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UPKLocalizationManager : NSObject

/**
 *  The method to get localized string by its key.
 *
 *  @param key The key of localized string.
 *
 *  @return Localized string for the key specified.
 */
- (NSString *) localizedStringForKey: (NSString *) key;

@end
