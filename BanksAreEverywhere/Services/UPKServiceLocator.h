//
//  UPKServiceLocator.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#define BANKS_SERVICE [[UPKServiceLocator sharedLocator] banksService]
#define LOCALIZATION_MANAGER [[UPKServiceLocator sharedLocator] localizationManager]
#define URL_MANAGER [[UPKServiceLocator sharedLocator] urlManager]
#define LOGGER [[UPKServiceLocator sharedLocator] logger]

#define UPKLocalizedString(key) [LOCALIZATION_MANAGER localizedStringForKey: key]

#ifdef DEBUG
#define UPKLog(...) [LOGGER logWithMethod: __PRETTY_FUNCTION__ line: __LINE__ format: __VA_ARGS__]
#else
#define UPKLog(...)
#endif

@class UPKBanksService;
@class UPKLocalizationManager;
@class UPKURLManager;
@class UPKLogger;


@interface UPKServiceLocator : NSObject

/**
 *  Shared instance of the locator that is intended to be used in any other class.
 *
 *  @return The shared instance of service locator.
 */
+ (nonnull UPKServiceLocator *) sharedLocator;


/**
 *  The banks service that handles all network activity.
 *
 *  @return The instance of UPKBanksService owned by the locator.
 */
- (nonnull UPKBanksService *) banksService;

/**
 *  The localization manager that helps in internalization.
 *
 *  @return The instance of UPKLocalizationManager owned by the locator.
 */
- (nonnull UPKLocalizationManager *) localizationManager;

/**
 *  The URLmanager that helps in building correct urls.
 *
 *  @return The instance of UPKURLManager owned by the locator.
 */
- (nonnull UPKURLManager *) urlManager;

/**
 *  The object that must be used for all log actions in the application.
 *
 *  @return The instance of Logger owned by the locator.
 */
- (nonnull UPKLogger *) logger;

@end
