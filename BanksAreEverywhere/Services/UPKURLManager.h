//
//  UPKURLManager.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@interface UPKURLManager : NSObject

- (nonnull NSURL *) banksCitiesURL;
- (nonnull NSURL *) banksFilialsURLWithParameters: (nonnull NSDictionary <NSString * , NSString *> *) parameters;

@end
