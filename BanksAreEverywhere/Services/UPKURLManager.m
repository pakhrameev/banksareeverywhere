//
//  UPKURLManager.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKURLManager.h"


#pragma mark - Locals

static NSString *const kBaseScheme = @"http";
static NSString *const kBaseHost = @"seekrate.ru";
static NSString *const kApiPathComponent = @"/api/";
static NSString *const kCitiesPathComponent = @"city.php";
static NSString *const kBankFilialsPathComponent = @"v3.php";


#pragma mark - UPKURLManager implementation

@implementation UPKURLManager

- (NSURLComponents *) baseURLComponents
{
  NSURLComponents *components = [[NSURLComponents alloc] init];
  components.scheme = kBaseScheme;
  components.host = kBaseHost;
  return components;
}

- (NSURLComponents *) baseApiURLComponents
{
  NSURLComponents *components = [self baseURLComponents];
  components.path = kApiPathComponent;
  return components;
}

- (NSURL *) baseApiURL
{
  return [self baseApiURLComponents].URL;
}


#pragma mark - Public methods

- (nonnull NSURL *) banksCitiesURL
{
  NSURL *citiesURL = [NSURL URLWithString: kCitiesPathComponent
                            relativeToURL: [self baseApiURL]];
  
  return citiesURL;
}

- (nonnull NSURL *) banksFilialsURLWithParameters: (NSDictionary <NSString *, NSString *> *) parameters
{
  NSURL *bankFilialsURL = [NSURL URLWithString: kBankFilialsPathComponent
                                 relativeToURL: [self baseApiURL]];
  
  NSURLComponents *components = [NSURLComponents componentsWithURL: bankFilialsURL
                                           resolvingAgainstBaseURL: YES];
  
  NSMutableArray <NSURLQueryItem *> *queryItems = [NSMutableArray new];
  [parameters enumerateKeysAndObjectsUsingBlock: ^(NSString * _Nonnull key,
                                                  NSString * _Nonnull obj,
                                                  BOOL * _Nonnull stop)
  {
    NSURLQueryItem *item = [NSURLQueryItem queryItemWithName: key value: obj];
    [queryItems addObject: item];
  }];
  
  components.queryItems = queryItems;
  
  return components.URL;
}

@end
