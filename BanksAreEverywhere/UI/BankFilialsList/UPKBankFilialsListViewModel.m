//
//  UPKBankFilialsListViewModel.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKBankFilialsListViewModel.h"

#import "UPKCity.h"
#import "UPKBankFilial.h"
#import "UPKBanksService.h"


@interface UPKBankFilialsListViewModel ()

@property (nonatomic, assign) NSUInteger bankFilialsRequestId;

@property (nonatomic, copy, nullable) NSArray <UPKBankFilial *> *bankFilials;
@property (nonatomic, copy, nullable) NSError *error;

@end


@implementation UPKBankFilialsListViewModel

@synthesize bankFilialsRequestId = m_bankFilialsRequestId;
@synthesize city = m_city;

- (void) setCity: (UPKCity *) city
{
  NSString *identifier = city.identifier;
  if (![self.city.identifier isEqualToString: identifier])
  {
    m_city = city;
    
    [self loadBankFilials];
  }
  else
  {
    m_city = city;
  }
}

- (void) setBankFilialsRequestId: (NSUInteger) bankFilialsRequestId
{
  m_bankFilialsRequestId = bankFilialsRequestId;
  
  if ([self loadingBankFilials])
  {
    [self.delegate bankFilialsStartedLoading: self];
  }
  else
  {
    [self.delegate bankFilialsFinishedLoading: self];
  }
}

- (BOOL) loadingBankFilials
{
  return (self.bankFilialsRequestId != kInvalidRequestId);
}

#pragma mark - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_bankFilialsRequestId = kInvalidRequestId;
  }
  return self;
}

#pragma mark - Public methods

- (void) loadBankFilials
{
  if ([self loadingBankFilials] || self.city == nil)
  {
    return;
  }
  
  self.error = nil;
  
  __weak UPKBankFilialsListViewModel *weakSelf = self;
  self.bankFilialsRequestId = [BANKS_SERVICE requestBankFilialsWithCity: self.city
                                                          andCompletion: ^(NSArray <UPKBankFilial *> * _Nullable bankFilials,
                                                                           NSUInteger requestId,
                                                                           NSError * _Nullable error)
                          {
                            dispatch_async(dispatch_get_main_queue(), ^{
                              
                              UPKBankFilialsListViewModel *strongSelf = weakSelf;
                              if (strongSelf == nil || requestId != strongSelf.bankFilialsRequestId)
                              {
                                return;
                              }
                              if (error.code == kNoInternetConnectionErrorCode)
                              {
                                strongSelf.error = error;
                              }
                              else
                              {
                                strongSelf.error = error;
                                strongSelf.bankFilials = [bankFilials copy];
                              }
                              strongSelf.bankFilialsRequestId = kInvalidRequestId;
                            });
                          }];
}

@end
