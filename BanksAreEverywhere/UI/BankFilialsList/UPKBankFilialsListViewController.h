//
//  UPKBankFilialsListViewController.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@class UPKCity;

@interface UPKBankFilialsListViewController : UITableViewController

@property (nonatomic, strong, nullable) UPKCity *city;

@end
