//
//  UPKBankFilialsListViewModel.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@class UPKCity;
@class UPKBankFilial;
@protocol UPKBankFilialsListViewModelDelegate;

@interface UPKBankFilialsListViewModel : NSObject

@property (nonatomic, weak, nullable) id <UPKBankFilialsListViewModelDelegate> delegate;
@property (nonatomic, strong, nullable) UPKCity *city;
@property (nonatomic, assign, readonly) BOOL loadingBankFilials;
@property (nonatomic, copy, nullable, readonly) NSArray <UPKBankFilial *> *bankFilials;
@property (nonatomic, copy, nullable, readonly) NSError *error;

- (void) loadBankFilials;

@end


@protocol UPKBankFilialsListViewModelDelegate <NSObject>

- (void) bankFilialsStartedLoading: (nonnull UPKBankFilialsListViewModel *) sender;
- (void) bankFilialsFinishedLoading: (nonnull UPKBankFilialsListViewModel *) sender;

@end
