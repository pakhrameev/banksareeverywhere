//
//  UPKBankFilialInfoTableViewCell.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UPKBankFilialInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@end
