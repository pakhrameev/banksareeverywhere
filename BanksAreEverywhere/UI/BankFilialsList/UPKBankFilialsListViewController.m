//
//  UPKBankFilialsListViewController.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKBankFilialsListViewController.h"
#import "UPKBankFilialsListViewModel.h"

#import "UPKCity.h"
#import "UPKBank.h"
#import "UPKBankFilial.h"

#import "UPKLoadingTableView.h"
#import "UPKBankFilialInfoTableViewCell.h"


@interface UPKBankFilialsListViewController () <UPKBankFilialsListViewModelDelegate>

@property (nonatomic, strong, readonly) UPKBankFilialsListViewModel *viewModel;

@end



#pragma mark - UPKBankFilialsListViewController implementation

@implementation UPKBankFilialsListViewController


#pragma mark - Properties

@synthesize viewModel = m_viewModel;

- (UPKCity *) city
{
  return self.viewModel.city;
}

- (void) setCity: (UPKCity *) city
{
  self.viewModel.city = city;
}

#pragma mark - Inits

- (instancetype) initWithStyle: (UITableViewStyle) style
{
  if (self = [super initWithStyle: style])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithNibName: (NSString *) nibNameOrNil
                          bundle: (NSBundle *) nibBundleOrNil
{
  if (self = [super initWithNibName: nibNameOrNil
                             bundle: nibBundleOrNil])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithCoder: (NSCoder *) aDecoder
{
  if (self = [super initWithCoder: aDecoder])
  {
    [self configure];
  }
  return self;
}


#pragma mark - Overrides

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];
  
  [self.viewModel loadBankFilials];
}

- (void) didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  
  // I don't need to write code here - as we discussed.
}

#pragma mark - Internal methods

- (void) configure
{
  m_viewModel = [[UPKBankFilialsListViewModel alloc] init];
  m_viewModel.delegate = self;
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView
{
  return 1;
}

- (NSInteger) tableView: (UITableView *) tableView
  numberOfRowsInSection: (NSInteger) section
{
  return self.viewModel.bankFilials.count;
}

- (UITableViewCell *) tableView: (UITableView *) tableView
          cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  static NSString * const kBankFilialCellReuseIdentifier = @"bankFilialReuseIdentifier";
  UPKBankFilialInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: kBankFilialCellReuseIdentifier
                                                          forIndexPath: indexPath];
  UPKBankFilial *bankFilial = self.viewModel.bankFilials[indexPath.row];
  cell.bankTitleLabel.text = bankFilial.bank.displayName;
  cell.addressLabel.text = bankFilial.address;
  cell.phoneNumberLabel.text = bankFilial.phone;
  
  return cell;
}


#pragma mark - UPKBankFilialsListViewModelDelegate

- (void) bankFilialsStartedLoading: (UPKBankFilialsListViewModel *) sender
{
  if ([self.tableView isKindOfClass: [UPKLoadingTableView class]])
  {
    UPKLoadingTableView *tableView = (UPKLoadingTableView *)self.tableView;
    [tableView showLoadingIndicator];
  }
  
  //update error view if it is persistent
}

- (void) bankFilialsFinishedLoading: (UPKBankFilialsListViewModel *) sender
{
  if ([self.tableView isKindOfClass: [UPKLoadingTableView class]])
  {
    UPKLoadingTableView *tableView = (UPKLoadingTableView *)self.tableView;
    [tableView hideLoadingIndicator];
  }
  
  [self.tableView reloadData];
  
  if (self.viewModel.error != nil)
  {
    [[[UIAlertView alloc] initWithTitle: UPKLocalizedString(@"ERROR_TITLE")
                                message: self.viewModel.error.localizedDescription
                               delegate: nil
                      cancelButtonTitle: UPKLocalizedString(@"OK_TITLE")
                      otherButtonTitles: nil] show];
  }
}

@end
