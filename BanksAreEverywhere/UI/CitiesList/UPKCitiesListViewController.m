//
//  UPKCitiesListViewController.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKCitiesListViewController.h"

#import "UPKCitiesListViewModel.h"
#import "UPKLoadingTableView.h"

#import "UPKBankFilialsListViewController.h"

#import "UPKCity.h"


#pragma mark - Locals

static NSString *const kBankFilialsSeagueIdentifier = @"bankFilialsSeagueIdentifier";


@interface UPKCitiesListViewController () <UPKCitiesListViewModelDelegate>

@property (nonatomic, strong, readonly) UPKCitiesListViewModel *viewModel;

@end

@implementation UPKCitiesListViewController

@synthesize viewModel = m_viewModel;


#pragma mark - Inits

- (instancetype) initWithStyle: (UITableViewStyle) style
{
  if (self = [super initWithStyle: style])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithNibName: (NSString *) nibNameOrNil
                          bundle: (NSBundle *) nibBundleOrNil
{
  if (self = [super initWithNibName: nibNameOrNil
                             bundle: nibBundleOrNil])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithCoder: (NSCoder *) aDecoder
{
  if (self = [super initWithCoder: aDecoder])
  {
    [self configure];
  }
  return self;
}

#pragma mark - Overrides

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];
  
  [self.viewModel loadCities];
}

- (void) didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  
  // As we discussed, no need to write code here
}


#pragma mark - Internal methods

- (void) configure
{
  m_viewModel = [[UPKCitiesListViewModel alloc] init];
  m_viewModel.delegate = self;
}

- (UPKCity *) cityAtIndexPath: (NSIndexPath *) indexPath
{
  NSString *key = self.viewModel.sortedLetters[indexPath.section];
  return self.viewModel.citiesByLetter[key][indexPath.row];
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView
{
    return self.viewModel.sortedLetters.count;
}

- (NSInteger) tableView: (UITableView *) tableView
  numberOfRowsInSection: (NSInteger) section
{
  NSString *key = self.viewModel.sortedLetters[section];
  return self.viewModel.citiesByLetter[key].count;
}

- (UITableViewCell *) tableView: (UITableView *) tableView
          cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  static NSString * const kCityCellReuseIdentifier = @"cityReuseIdentifier";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: kCityCellReuseIdentifier
                                                          forIndexPath: indexPath];
  cell.textLabel.text = [self cityAtIndexPath: indexPath].displayName;
  
  return cell;
}

- (NSString *) tableView: (UITableView *) tableView
 titleForHeaderInSection: (NSInteger) section
{
  return self.viewModel.sortedLetters[section];
}

- (nullable NSArray <NSString *> *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
  return self.viewModel.sortedLetters;
}

- (NSInteger)       tableView: (UITableView *) tableView
  sectionForSectionIndexTitle: (NSString *) title
                      atIndex: (NSInteger) index
{
  return [self.viewModel.sortedLetters indexOfObject: title];
}

#pragma mark - Table view delegate

- (void)        tableView: (UITableView *) tableView
  didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  [tableView deselectRowAtIndexPath: indexPath animated: YES];
  
  self.viewModel.selectedCity = [self cityAtIndexPath: indexPath];
  
  [self performSegueWithIdentifier: kBankFilialsSeagueIdentifier
                            sender: self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
  if ([segue.identifier isEqualToString: kBankFilialsSeagueIdentifier])
  {
    UIViewController *vc = [segue destinationViewController];
    NSAssert ([vc isKindOfClass: [UPKBankFilialsListViewController class]], @"I expect UPKBankFilialsListViewController in this segue");
    UPKBankFilialsListViewController *bankFilialsVC = (UPKBankFilialsListViewController *) vc;
    bankFilialsVC.city = self.viewModel.selectedCity;
  }
  
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
}

#pragma mark - UPKCitiesListViewModelDelegate implementation

- (void) citiesStartedLoading: (UPKCitiesListViewModel *) sender
{
  if ([self.tableView isKindOfClass: [UPKLoadingTableView class]])
  {
    UPKLoadingTableView *tableView = (UPKLoadingTableView *)self.tableView;
    [tableView showLoadingIndicator];
  }
  
  //update error view if it is persistent
}

- (void) citiesFinishedLoading: (UPKCitiesListViewModel *) sender
{
  if ([self.tableView isKindOfClass: [UPKLoadingTableView class]])
  {
    UPKLoadingTableView *tableView = (UPKLoadingTableView *)self.tableView;
    
    [tableView hideLoadingIndicator];
  }
  
  [self.tableView reloadData];
  
  if (self.viewModel.error != nil)
  {
    [[[UIAlertView alloc] initWithTitle: UPKLocalizedString(@"ERROR_TITLE")
                                message: self.viewModel.error.localizedDescription
                               delegate: nil
                      cancelButtonTitle: UPKLocalizedString(@"OK_TITLE")
                      otherButtonTitles: nil] show];
  }
}

@end
