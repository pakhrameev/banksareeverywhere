//
//  UPKCitiesListViewModel.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@class UPKCity;
@protocol UPKCitiesListViewModelDelegate;


@interface UPKCitiesListViewModel : NSObject

@property (nonatomic, weak, nullable) id <UPKCitiesListViewModelDelegate> delegate;
@property (nonatomic, strong, nullable) UPKCity *selectedCity;
@property (nonatomic, assign, readonly) BOOL loadingCities;
@property (nonatomic, copy, nullable, readonly) NSArray <UPKCity *> *cities;
@property (nonatomic, copy, nullable, readonly) NSDictionary <NSString *, NSArray <UPKCity *> *> *citiesByLetter;
@property (nonatomic, copy, nullable, readonly) NSArray <NSString *> * sortedLetters;
@property (nonatomic, copy, nullable, readonly) NSError *error;

- (void) loadCities;

@end


@protocol UPKCitiesListViewModelDelegate <NSObject>

- (void) citiesStartedLoading: (nonnull UPKCitiesListViewModel *) sender;
- (void) citiesFinishedLoading: (nonnull UPKCitiesListViewModel *) sender;

@end
