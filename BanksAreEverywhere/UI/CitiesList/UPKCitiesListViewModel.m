//
//  UPKCitiesListViewModel.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKCitiesListViewModel.h"

#import "UPKCity.h"
#import "UPKBanksService.h"


@interface UPKCitiesListViewModel ()

@property (nonatomic, assign) NSUInteger citiesRequestId;

@property (nonatomic, copy, nullable) NSArray <UPKCity *> *cities;
@property (nonatomic, copy, nullable) NSError *error;

@end


@implementation UPKCitiesListViewModel

@synthesize citiesRequestId = m_citiesRequestId;
@synthesize cities = m_cities;
@synthesize citiesByLetter = m_citiesByLetter;
@synthesize sortedLetters = m_sortedLetters;

- (void) setCities: (NSArray <UPKCity *> *) cities
{
  m_cities = [cities copy];
  
  [self updateCitiesByLetter];
}

- (void) setCitiesRequestId: (NSUInteger) citiesRequestId
{
  m_citiesRequestId = citiesRequestId;
  
  if ([self loadingCities])
  {
    [self.delegate citiesStartedLoading: self];
  }
  else
  {
    [self.delegate citiesFinishedLoading: self];
  }
}

- (BOOL) loadingCities
{
  return (self.citiesRequestId != kInvalidRequestId);
}

#pragma mark - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_citiesRequestId = kInvalidRequestId;
  }
  return self;
}


#pragma mark - Internal methods

- (void) updateCitiesByLetter
{
  NSMutableDictionary <NSString *, NSMutableArray <UPKCity *> *> *mdic = [NSMutableDictionary new];
  for (UPKCity *city in self.cities)
  {
    NSString *name = city.displayName;
    NSString *firstLetter = (name.length > 1) ? [name substringToIndex: 1] : name;
    NSMutableArray <UPKCity *> *mcities = [mdic objectForKey: firstLetter];
    if (mcities == nil)
    {
      mcities = [NSMutableArray new];
      [mdic setObject: mcities forKey: firstLetter];
    }
    [mcities addObject: city];
  }
  
  NSMutableDictionary <NSString *, NSArray <UPKCity *> *> *mcitiesByLetter = [NSMutableDictionary new];
  [mdic enumerateKeysAndObjectsUsingBlock: ^(NSString * _Nonnull key,
                                             NSMutableArray <UPKCity *> * _Nonnull obj,
                                             BOOL * _Nonnull stop) {
    [mcitiesByLetter setObject: obj.copy forKey: key];
  }];
  
  m_citiesByLetter = mcitiesByLetter.copy;
  
  [self updateSortedLetters];
}

- (void) updateSortedLetters
{
  m_sortedLetters = [[self.citiesByLetter allKeys] sortedArrayUsingSelector: @selector(localizedCaseInsensitiveCompare:)];
}


#pragma mark - Public methods

- (void) loadCities
{
  if ([self loadingCities])
  {
    return;
  }
  
  self.error = nil;
  
  __weak UPKCitiesListViewModel *weakSelf = self;
  self.citiesRequestId = [BANKS_SERVICE requestCitiesWithCompletion: ^(NSArray <UPKCity *> * _Nullable cities,
                                                                       NSUInteger requestId,
                                                                       NSError * _Nullable error)
  {
    dispatch_async(dispatch_get_main_queue(), ^{
      
      UPKCitiesListViewModel *strongSelf = weakSelf;
      if (strongSelf == nil || requestId != strongSelf.citiesRequestId)
      {
        return;
      }
      if (error.code == kNoInternetConnectionErrorCode)
      {
        strongSelf.error = error;
      }
      else
      {
        strongSelf.error = error;
        strongSelf.cities = [cities copy];
      }
      strongSelf.citiesRequestId = kInvalidRequestId;
    });
  }];
}

@end
