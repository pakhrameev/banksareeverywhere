//
//  UPKLoadingTableView.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

@interface UPKLoadingTableView : UITableView

- (void) showLoadingIndicator;
- (void) hideLoadingIndicator;

@end
