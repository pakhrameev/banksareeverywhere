//
//  UPKLoadingTableView.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 01.02.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "UPKLoadingTableView.h"


@interface UPKLoadingTableView ()

@property (nonatomic, strong) UIActivityIndicatorView *loadingView;

@end



@implementation UPKLoadingTableView


#pragma mark - Inits

- (instancetype) initWithCoder: (NSCoder *) aDecoder
{
  if (self = [super initWithCoder: aDecoder])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithFrame: (CGRect) frame
{
  if (self = [super initWithFrame: frame])
  {
    [self configure];
  }
  return self;
}

- (instancetype) initWithFrame: (CGRect) frame style: (UITableViewStyle) style
{
  if (self = [super initWithFrame: frame style: style])
  {
    [self configure];
  }
  return self;
}


#pragma mark - Overrides

- (void) reloadData
{
  [super reloadData];
  
  [self bringSubviewToFront: self.loadingView];
}


#pragma mark - Internal methods

- (void) configure
{
  self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
  
  [self addSubview: self.loadingView];
  
  self.loadingView.translatesAutoresizingMaskIntoConstraints = NO;
  
  [self addConstraint: [NSLayoutConstraint constraintWithItem: self.loadingView
                                                    attribute: NSLayoutAttributeCenterX
                                                    relatedBy: NSLayoutRelationEqual
                                                       toItem: self
                                                    attribute: NSLayoutAttributeCenterX
                                                   multiplier: 1.f
                                                     constant: 0.f]];
  [self addConstraint: [NSLayoutConstraint constraintWithItem: self.loadingView
                                                    attribute: NSLayoutAttributeCenterY
                                                    relatedBy: NSLayoutRelationEqual
                                                       toItem: self
                                                    attribute: NSLayoutAttributeCenterY
                                                   multiplier: 1.f
                                                     constant: 0.f]];
  const CGFloat width = self.loadingView.frame.size.width;
  [self addConstraint: [NSLayoutConstraint constraintWithItem: self.loadingView
                                                    attribute: NSLayoutAttributeWidth
                                                    relatedBy: NSLayoutRelationEqual
                                                       toItem: nil
                                                    attribute: NSLayoutAttributeNotAnAttribute
                                                   multiplier: 1.f
                                                     constant: width]];
  const CGFloat height = self.loadingView.frame.size.height;
  [self addConstraint: [NSLayoutConstraint constraintWithItem: self.loadingView
                                                    attribute: NSLayoutAttributeHeight
                                                    relatedBy: NSLayoutRelationEqual
                                                       toItem: nil
                                                    attribute: NSLayoutAttributeNotAnAttribute
                                                   multiplier: 1.f
                                                     constant: height]];
  
  self.loadingView.color = [UIColor blackColor];
  
  self.tableFooterView = [UIView new];
}


#pragma mark - Public methods

- (void) showLoadingIndicator
{
  self.loadingView.hidden = NO;
  [self bringSubviewToFront: self.loadingView];
  
  [self.loadingView startAnimating];
}

- (void) hideLoadingIndicator
{
  self.loadingView.hidden = YES;
  
  [self.loadingView stopAnimating];
}

@end
