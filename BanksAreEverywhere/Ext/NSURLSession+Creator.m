//
//  NSURLSession+Creator.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "NSURLSession+Creator.h"

@implementation NSURLSession (Creator)

+ (NSURLSession *) sessionWithDelegate: (id <NSURLSessionDelegate>) delegate
{
  NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
  config.timeoutIntervalForRequest = 30.0;
  config.HTTPCookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  config.HTTPShouldSetCookies = YES;
  
  NSOperationQueue *delegateQueue = [NSOperationQueue new];
  delegateQueue.maxConcurrentOperationCount = 1;
  
  return [NSURLSession sessionWithConfiguration: config
                                       delegate: delegate
                                  delegateQueue: delegateQueue];
}

@end
