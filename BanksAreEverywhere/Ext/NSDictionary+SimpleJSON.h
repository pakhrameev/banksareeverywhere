//
//  NSDictionary+SimpleJSON.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Extension that simplifies JSON response parsing.
 */
@interface NSDictionary (SimpleJSON)

/**
 *  Gets object of the specified type from the specified path.
 *
 *  @param type Type of the resulting object. Nil skips type check;
 *  @param path String formatted as in example: dict.array[index].fieldName
 *
 *  @return Result with given type or nil if path not found or there's type mismatch.
 */
- (id) objectWithType: (Class) type fromPath: (NSString *) path;

/**
 *  Convenience methods for previous one.
 */
- (NSString *) stringFromPath: (NSString *) path;
- (NSNumber *) numberFromPath: (NSString *) path;
- (NSArray *) arrayFromPath: (NSString *) path;
- (NSDictionary *) dictionaryFromPath: (NSString *) path;
- (id) objectFromPath: (NSString *) path;

@end

