//
//  NSURLSession+Creator.h
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLSession (Creator)

/*
 *  Creates url session with default settings and on serial callback queue.
 */
+ (NSURLSession *) sessionWithDelegate: (id <NSURLSessionDelegate>) delegate;

@end
