//
//  NSDictionary+SimpleJSON.m
//  BanksAreEverywhere
//
//  Created by Pavel Akhrameev on 31.01.16.
//  Copyright © 2016 UP. All rights reserved.
//

#import "NSDictionary+SimpleJSON.h"


#pragma mark - NSDictionary (SimpleJSON) implementation

@implementation NSDictionary (SimpleJSON)

#pragma mark - Public methods

- (id) objectWithType: (Class) type fromPath: (NSString *) path
{
  NSArray *components = [path componentsSeparatedByString: @"."];
  NSDictionary *currentDict = self;
  NSCharacterSet *bracketsSet = [NSCharacterSet characterSetWithCharactersInString: @"[]"];
  for (NSString *component in components)
  {
    NSArray *bracketsComponents = [component componentsSeparatedByCharactersInSet: bracketsSet];
    // filters out empty components, no idea why componentsSeparatedByCharactersInSet: produces them
    NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K > 0", @"length"];
    bracketsComponents = [bracketsComponents filteredArrayUsingPredicate: pred];
    if (bracketsComponents.count == 1)
    {
      if (component == [components lastObject])
      {
        id result = currentDict[component];
        if (type != nil && ![result isKindOfClass: type])
        {
          return nil;
        }
        
        return result;
      }
      else
      {
        currentDict = currentDict[component];
        if (![currentDict isKindOfClass: [NSDictionary class]])
        {
          return nil;
        }
      }
    }
    else if (bracketsComponents.count == 2)
    {
      NSArray *array = currentDict[[bracketsComponents firstObject]];
      if (![array isKindOfClass: [NSArray class]])
      {
        return nil;
      }
      
      NSInteger index = [[bracketsComponents lastObject] integerValue];
      if (index == -1)
      {
        index = array.count - 1;
      }
      
      if (index < 0 || index >= array.count)
      {
        return nil;
      }
      
      if (component == [components lastObject])
      {
        id result = array[index];
        if (type != nil && ![result isKindOfClass: type])
        {
          return nil;
        }
        
        return result;
      }
      else
      {
        currentDict = array[index];
        if (![currentDict isKindOfClass: [NSDictionary class]])
        {
          return nil;
        }
      }
    }
    else
    {
      return nil;
    }
  }
  
  return nil;
}

- (NSString *) stringFromPath: (NSString *) path
{
  return [self objectWithType: [NSString class] fromPath: path];
}

- (NSNumber *) numberFromPath: (NSString *) path
{
  return [self objectWithType: [NSNumber class] fromPath: path];
}

- (NSArray *) arrayFromPath: (NSString *) path
{
  return [self objectWithType: [NSArray class] fromPath: path];
}

- (NSDictionary *) dictionaryFromPath: (NSString *) path
{
  return [self objectWithType: [NSDictionary class] fromPath: path];
}

- (id) objectFromPath: (NSString *) path
{
  return [self objectWithType: nil fromPath: path];
}

@end

